const form = document.querySelector(".php-email-form form"),
continueBtn = form.querySelector("button"),
errorText = form.querySelector(".error-message");
successText = form.querySelector(".sent-message");
loadingText = form.querySelector(".loading");



// function invoicePage (){

//                   location.href="invoice.php";
// }



form.onsubmit = (e)=>{
    e.preventDefault();
}

continueBtn.onclick = ()=>{
    let formData = new FormData(form);
    

    // let xhs = new XMLHttpRequest();
    
    // xhs.open("POST", "https://script.google.com/macros/s/id/exec", true);
   
    // xhs.send(formData);

    let xhr = new XMLHttpRequest();
    // xhr.open("POST", "php/sendinquiry.php", true);
    xhr.open("POST", "forms/contact.php", true);
    
    xhr.onload = ()=>{
      if(xhr.readyState === XMLHttpRequest.DONE){
          if(xhr.status === 200){
              let data = xhr.response;
              if(data === "success"){
                

                successText.style.display = "block";
                
                successText.textContent = "Message has been sent";
                errorText.style.display = "none";
               
                // setTimeout(invoicePage, 5000);
                

              }else{
                errorText.style.display = "block";
                errorText.textContent = data;
                successText.style.display = "none";
              }
          }
      }
    }
    
    xhr.send(formData);

}